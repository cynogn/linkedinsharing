package com.gautam.linkedinshare;

import java.util.ArrayList;
import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.gautam.constants.CONSTANTS;
import com.gautam.credentials.LinkedInCredentails;
import com.google.code.linkedinapi.client.LinkedInApiClientFactory;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthServiceFactory;

/**
 * Autentication Dialog class
 * 
 * @author gautam
 * 
 */
public class LinkedinDialog extends Dialog {
	private ProgressBar progressBar;

	public LinkedinDialog(Context context) {
		super(context);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);// must call before super.
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ln_dialog);
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		setWebView();
	}

	/**
	 * set webview.
	 */
	private void setWebView() {

		new ObtainToken().execute();
	}

	/**
	 * webview client for internal url loading
	 */
	class HelloWebViewClient extends WebViewClient {
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			/**
			 * Enables probress bar on page load
			 */
			progressBar.setVisibility(View.VISIBLE);
			super.onPageStarted(view, url, favicon);
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			/**
			 * Hides the progrss bar after the laoding the complete page
			 */
			progressBar.setVisibility(View.INVISIBLE);
			super.onPageFinished(view, url);
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (url.contains(CONSTANTS.OAUTH_CALLBACK_URL)) {
				Uri uri = Uri.parse(url);
				String verifier = uri.getQueryParameter("oauth_verifier");
				cancel();

				for (OnVerifyListener d : listeners) {
					d.onVerify(verifier);
				}
			} else {
				view.loadUrl(url);
			}

			return true;
		}
	}

	/**
	 * List of listener.
	 */
	private List<OnVerifyListener> listeners = new ArrayList<OnVerifyListener>();

	/**
	 * Register a callback to be invoked when authentication have finished.
	 * 
	 * @param data
	 *            The callback that will run
	 */
	public void setVerifierListener(OnVerifyListener data) {
		listeners.add(data);
	}

	/**
	 * Listener for oauth_verifier.
	 */
	interface OnVerifyListener {
		/**
		 * invoked when authentication have finished.
		 * 
		 * @param verifier
		 *            oauth_verifier code.
		 */
		public void onVerify(String verifier);
	}

	class ObtainToken extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			LinkedInCredentails.getInstance();
			LinkedInCredentails.getInstance().setoAuthService(
					LinkedInOAuthServiceFactory.getInstance()
							.createLinkedInOAuthService(
									CONSTANTS.LINKEDIN_CONSUMER_KEY,
									CONSTANTS.LINKEDIN_CONSUMER_SECRET));

			LinkedInCredentails.getInstance();
			LinkedInCredentails.getInstance().setFactory(
					LinkedInApiClientFactory.newInstance(
							CONSTANTS.LINKEDIN_CONSUMER_KEY,
							CONSTANTS.LINKEDIN_CONSUMER_SECRET));

			LinkedInCredentails.getInstance()
					.setLiToken(
							LinkedInCredentails
									.getInstance()
									.getoAuthService()
									.getOAuthRequestToken(
											CONSTANTS.OAUTH_CALLBACK_URL));

			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			WebView mWebView = (WebView) findViewById(R.id.webkitWebView1);
			mWebView.getSettings().setJavaScriptEnabled(true);

			Log.i("LinkedinSample", LinkedInCredentails.getInstance()
					.getLiToken().getAuthorizationUrl());
			mWebView.loadUrl(LinkedInCredentails.getInstance().getLiToken()
					.getAuthorizationUrl());
			mWebView.setWebViewClient(new HelloWebViewClient());

			super.onPostExecute(result);
		}
	}
}