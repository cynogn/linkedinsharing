package com.gautam.linkedinshare;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gautam.constants.CONSTANTS;
import com.gautam.credentials.LinkedInCredentails;
import com.gautam.linkedinshare.LinkedinDialog.OnVerifyListener;
import com.google.code.linkedinapi.client.LinkedInApiClient;
import com.google.code.linkedinapi.client.LinkedInApiClientFactory;
import com.google.code.linkedinapi.client.oauth.LinkedInAccessToken;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthService;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthServiceFactory;
import com.google.code.linkedinapi.schema.Person;

/**
 * LinkedInSampleActivity - Linkedin Sharing activity
 * 
 * @author Gautam
 */
public class LinkedInSampleActivity extends Activity implements OnClickListener {

	/**
	 * Login Button
	 */
	private Button mLogin;
	/**
	 * Share Button
	 */
	private Button mShare;
	/**
	 * Status Message
	 */
	private EditText mLinkedInStatus;
	/**
	 * Username
	 */
	private TextView mUserName;
	/**
	 * LinkedInClient
	 */
	private LinkedInApiClient mLinkedInClient;
	/**
	 * access Token
	 */
	private LinkedInAccessToken mAccessToken = null;
	/*
	 * hard-coded strings
	 */
	public static final String SHARED_SUCCESS_MESSAGE = "Shared sucessfully";
	public static final String THROTTLE_LIMIT_EXCEPTION = "Reached Throttle limit!";
	public static final String MESSAGE_VALIDATEION_MESSAGE = "Please enter the text to share";
	public static final String NO_INTERNET_CONNECTION_MESSAGE = "No internet connection available!";

	public static final String OAUTH_CALLBACK_HOST = "litestcalback";

	/**
	 * OAuth service Token
	 */
	final LinkedInOAuthService mOAuthService = LinkedInOAuthServiceFactory
			.getInstance().createLinkedInOAuthService(
					CONSTANTS.LINKEDIN_CONSUMER_KEY,
					CONSTANTS.LINKEDIN_CONSUMER_SECRET);
	/**
	 * OAuth factory
	 */
	final LinkedInApiClientFactory mFactory = LinkedInApiClientFactory
			.newInstance(CONSTANTS.LINKEDIN_CONSUMER_KEY,
					CONSTANTS.LINKEDIN_CONSUMER_SECRET);

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initaliseAndSetListeners();

	}

	/**
	 * Initalizing the UI elements and the listeners
	 */
	private void initaliseAndSetListeners() {
		/**
		 * Initalizing the Views
		 */
		mShare = (Button) findViewById(R.id.share);
		mUserName = (TextView) findViewById(R.id.name);
		mLinkedInStatus = (EditText) findViewById(R.id.et_share);
		mLogin = (Button) findViewById(R.id.login);
		/**
		 * initalizing the button liteners
		 */
		mLogin.setOnClickListener(this);
		mShare.setOnClickListener(this);

	}

	/**
	 * Authenticates the user
	 */
	void authenticateUser() {
		/**
		 * Custom Authentication Dialog
		 */
		LinkedinDialog mAuthenticationDialog = new LinkedinDialog(
				LinkedInSampleActivity.this);
		/**
		 * Callback after vertification
		 */
		mAuthenticationDialog.setVerifierListener(new OnVerifyListener() {

			@Override
			public void onVerify(String verifier) {
				new RetriveLinkedInData().execute(verifier);
			}
		});
		/**
		 * code to make the editTextboxes move up as the keyboard pops up
		 */
		mAuthenticationDialog.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		mAuthenticationDialog.show();
	}

	/**
	 * Async. Task to retrive the data from the Linkedin to out app
	 * 
	 * @author gautam
	 * 
	 */
	class RetriveLinkedInData extends AsyncTask<String, Void, Void> {
		private ProgressDialog progressDialog;
		private Person person;

		@Override
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(LinkedInSampleActivity.this);
			progressDialog.setTitle("Please wait");
			progressDialog.setMessage("Loading...");
			progressDialog.setCancelable(true);
			progressDialog.show();

			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(Void result) {
			if (person != null) {
				mUserName.setText("Welcome " + person.getFirstName()
						+ person.getLastName() + " ! ");
				mLinkedInStatus.setText("http://coderwall.com/wannabegeekboy");
				mUserName.setVisibility(0);
				mLogin.setVisibility(4);
				mShare.setVisibility(0);
				mLinkedInStatus.setVisibility(0);
			}
			if (progressDialog != null)
				progressDialog.dismiss();

			super.onPostExecute(result);
		}

		@Override
		protected Void doInBackground(String... params) {
			LinkedInCredentails linkedInCredentails = LinkedInCredentails
					.getInstance();
			try {
				mAccessToken = linkedInCredentails.getoAuthService()
						.getOAuthAccessToken(linkedInCredentails.getLiToken(),
								params[0]);
				linkedInCredentails.getFactory().createLinkedInApiClient(
						mAccessToken);
			} catch (Exception e) {
				person = null;

				LinkedInSampleActivity.this.runOnUiThread(new Runnable() {
					public void run() {
						Toast.makeText(LinkedInSampleActivity.this,
								"Authenticaition Cancelled", Toast.LENGTH_SHORT)
								.show();
					}
				});
				return null;
			}
			mLinkedInClient = mFactory.createLinkedInApiClient(mAccessToken);
			person = mLinkedInClient.getProfileForCurrentUser();
			return null;
		}
	}

	@Override
	public void onClick(View arg0) {
		/**
		 * Handing login button click
		 */
		if (arg0.getId() == R.id.login) {
			if (isOnline())
				authenticateUser();
		} else
		/**
		 * Handing share button click
		 */
		if (arg0.getId() == R.id.share) {
			System.out.println("Clicked share button");
			if (isOnline()) {
				final String share = mLinkedInStatus.getText().toString();
				if (null != share && !share.equalsIgnoreCase("")) {
					mLinkedInClient = mFactory
							.createLinkedInApiClient(mAccessToken);
					new Thread(new Runnable() {
						@Override
						public void run() {
							try {
								mLinkedInClient.postNetworkUpdate(share);

								LinkedInSampleActivity.this
										.runOnUiThread(new Runnable() {
											public void run() {
												Toast.makeText(
														LinkedInSampleActivity.this,
														SHARED_SUCCESS_MESSAGE,
														Toast.LENGTH_SHORT)
														.show();
											}
										});

							} catch (Exception e) {
								System.out
										.println("Reached the throttle limit");
								LinkedInSampleActivity.this
										.runOnUiThread(new Runnable() {
											public void run() {

												Toast.makeText(
														LinkedInSampleActivity.this,
														THROTTLE_LIMIT_EXCEPTION,
														Toast.LENGTH_SHORT)
														.show();
											}
										});

							}
						}
					}).start();
					mLinkedInStatus.setText("");
				} else {

					Toast.makeText(LinkedInSampleActivity.this,
							MESSAGE_VALIDATEION_MESSAGE, Toast.LENGTH_SHORT)
							.show();
				}
			}
		}
	}

	/**
	 * Checks internet connectivity
	 * 
	 * @return
	 */
	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		Toast.makeText(LinkedInSampleActivity.this,
				NO_INTERNET_CONNECTION_MESSAGE, Toast.LENGTH_SHORT).show();
		return false;
	}
}
