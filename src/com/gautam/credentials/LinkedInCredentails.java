package com.gautam.credentials;

import com.google.code.linkedinapi.client.LinkedInApiClientFactory;
import com.google.code.linkedinapi.client.oauth.LinkedInOAuthService;
import com.google.code.linkedinapi.client.oauth.LinkedInRequestToken;

/**
 * LinkedIn Login Credentails
 * 
 * @author gautam
 * 
 */
public class LinkedInCredentails {
	/**
	 * Linkedin factory
	 */
	LinkedInApiClientFactory factory;
	/**
	 * Linkedin Service
	 */
	LinkedInOAuthService oAuthService;
	/**
	 * LinkedIn token
	 */
	LinkedInRequestToken liToken;
	/**
	 * SingleTop class
	 */
	private static LinkedInCredentails singleInstance;

	private LinkedInCredentails() {
	}

	public static LinkedInCredentails getInstance() {
		if (singleInstance == null) {
			synchronized (LinkedInCredentails.class) {
				if (singleInstance == null) {
					singleInstance = new LinkedInCredentails();
				}
			}
		}
		return singleInstance;
	}

	public LinkedInApiClientFactory getFactory() {
		return factory;
	}

	public void setFactory(LinkedInApiClientFactory factory) {
		this.factory = factory;
	}

	public LinkedInOAuthService getoAuthService() {
		return oAuthService;
	}

	public void setoAuthService(LinkedInOAuthService oAuthService) {
		this.oAuthService = oAuthService;
	}

	public LinkedInRequestToken getLiToken() {
		return liToken;
	}

	public void setLiToken(LinkedInRequestToken liToken) {
		this.liToken = liToken;
	}
}
